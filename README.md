# ArchLinux Wiki

> 这个文档属于我个人自建使用，仅供学习！

> ArchLinux分支都可用

## [ArchLinux - 文档安装](https://gitee.com/auroot/arch_wiki/blob/master/ArchLinux/arch_install.md)

## [Auins - 脚本安装](https://gitee.com/auroot/Auins)

## [Conkyrc - 桌面监控](https://gitee.com/auroot/conkyrc)

## [Tools_Scripts - 工具脚本](https://gitee.com/auroot/arch_script)

## [Archlive - 镜像构建 停更](https://gitee.com/auroot/auroot-archlive)

## [Arch镜像站 - 国内](https://gitee.com/auroot/Auins/blob/master/arch-mirrors.md)

## [镜像站集合](https://gitee.com/auroot/arch_wiki/blob/master/mirrors-in-china.md)


![images](https://gitee.com/auroot/conkyrc/raw/master/conkyrc/icons/desktop.png)

